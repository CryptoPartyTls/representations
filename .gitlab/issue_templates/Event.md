# CryptoParty

## Contexte

**Durée et date :** *Coming soon*

**Lieu :** *Coming soon*

**Format :** *Coming soon*

**Description :** *Coming soon*


## Programme / Boite à idées

*Coming soon*


## Préparation

* [ ] Création de l'issue
* [ ] Définition du programme (J-30)
* [ ] Communication
    * [ ] Mails
    * [ ] Réseaux sociaux
    * [ ] Médias / Presse locale
* [ ] Répétitions
    * [ ] J-15
    * [ ] J-7
* [ ] Logistique
    * [ ] Disposition de la salle
    * [ ] Projection (PC ? TV  ? Vidéoprojecteur ? Connectique ?)
    * [ ] Enregistrement ?
* [ ] Rétrospective
    * [ ] Date : *Coming soon*
    * [ ] Lieu : *Coming soon* 


## Bilan

*Coming soon*


/label ~"Upcoming event" ~"Event"
